# keys

<div align="center">
  <img src="https://cobox.cloud/src/svg/logo.svg">
</div>

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

## Table of Contents

  - [About](#about)
  - [Install](#install)
  - [Usage](#usage)
  - [API](#api)
  - [Contributing](#contributing)
  - [License](#license)

## About
**CoBox** is an encrypted p2p file system and distributed back-up tool. [README](https://gitlab.com/coboxcoop/readme) provides a map of the project.

`keys` is a simple repository for standardising the way we save keys across the cobox stack, as well as a derivation function that can be used in different space types.

## Install
```
npm i -g @coboxcoop/keys
```

## Usage
Stores keys as files with permissions restricted to read-only, preventing the software programatically altering the key file once it exists.

```
const crypto = require('cobox-crypto')
const keys = require('cobox-keys')

let keyName = 'encryption_key'
let encryptionKey = keys.loadKey('path/to/key', keyName) || crypto.encryptionKey()

keys.saveKey('path/to/key', keyName, encryptionKey)
```

## API
See swagger documentation... (we won't have this for a while).

## Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

# License

[`AGPL-3.0-or-later`](./LICENSE)
