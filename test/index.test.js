const { describe } = require('tape-plus')
const { deriveFromStoredParent } = require('..')
const fs = require('fs')
const path = require('path')
const crypto = require('@coboxcoop/crypto')

const { tmp, cleanup } = require('./util')

describe('keys: DeriveKeyPair', (context) => {
  context('derives a keyPair from a saved parent_key', (assert, next) => {
    var storage = tmp()
    var deriveKeyPair = deriveFromStoredParent(storage, crypto.keyPair)
    var keyPair = deriveKeyPair(1, 'hello')
    assert.same(Buffer.byteLength(keyPair.publicKey), 32, 'generates a 32 byte public key')
    assert.same(Buffer.byteLength(keyPair.secretKey), 64, 'generates a 64 byte secret key')
    assert.ok(fs.existsSync(path.join(storage, 'parent_key')), 'creates a parent key')
    cleanup(storage, next)
  })
})

