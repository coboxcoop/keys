const mkdirp = require('mkdirp')
const fs = require('fs')
const path = require('path')
const assert = require('assert')
const crypto = require('@coboxcoop/crypto')
const sodium = require('sodium-native')

function loadKey (storage, keyName) {
  try { return fs.readFileSync(path.join(storage, keyName)) }
  catch (err) { return }
}

function saveKey (storage, keyName, key) {
  var keyPath = path.join(storage, keyName)
  if (!fs.existsSync(keyPath)) {
    mkdirp.sync(path.dirname(keyPath))
    fs.writeFileSync(keyPath, key, {
      mode: fs.constants.S_IRUSR
    })
  }

  assert(fs.existsSync(keyPath), `failed to store ${keyName}`)
}

function deriveFromStoredParent (storage, deriveKeyPair) {
  return function buildKeyPair (id, context) {
    var parentKey = loadKey(storage, 'parent_key') || crypto.masterKey()
    saveKey(storage, 'parent_key', parentKey)
    var keyPair = deriveKeyPair(parentKey, id, context)
    sodium.sodium_memzero(parentKey)
    return keyPair
  }
}

module.exports = {
  loadKey,
  saveKey,
  deriveFromStoredParent
}

